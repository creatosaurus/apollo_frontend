import CanvasData from '../CanvasData'

export const moveTheAnimationTopToBottomText = (text) => {
    CanvasData.previewCtx.font = text.size + "px Arial";
    CanvasData.previewCtx.fillStyle = text.color;
        if (text.y > text.animation.y) {
        CanvasData.previewCtx.fillText("Hello World", text.x, text.animation.y = text.animation.y + text.animation.speed);
    } else {
        CanvasData.previewCtx.fillText("Hello World", text.x, text.y);
    }
}

export const moveTheAnimationTopToBottomSvg = (svg) => {
    CanvasData.previewCtx.beginPath();
    CanvasData.previewCtx.fillStyle = svg.color;
    if (svg.y > svg.animation.y) {
        CanvasData.previewCtx.rect(svg.x, svg.animation.y = svg.animation.y + svg.animation.speed, svg.width, svg.height);
        CanvasData.previewCtx.fill();
    } else {
        CanvasData.previewCtx.rect(svg.x, svg.y, svg.width, svg.height);
        CanvasData.previewCtx.fill();
    }
}

export const moveTheAnimationTopToBottomImage = (image) => {
    var img = document.getElementById(image.elementId);
    if (image.y > image.animation.y) {
        CanvasData.previewCtx.drawImage(img, image.x, image.animation.y = image.animation.y + image.animation.speed, image.width / 3, image.height / 3);
    } else {
        CanvasData.previewCtx.drawImage(img, image.x, image.y, image.width / 3, image.height / 3);
    }
}

export const moveTheAnimationTopToBottomVideo = (data) => {
    var video = document.getElementById(data.elementId);
    if (data.y > data.animation.y) {
        CanvasData.previewCtx.drawImage(video, data.x, data.animation.y = data.animation.y + data.animation.speed, data.width / 3, data.height / 3);
    } else {
        CanvasData.previewCtx.drawImage(video, data.x, data.y, data.width / 3, data.height / 3);
    }
    video.play()
}