import CanvasData from '../CanvasData'

export const moveTheAnimationRightToLeftText = (text) => {
    CanvasData.previewCtx.font = text.size + "px Arial";
    CanvasData.previewCtx.fillStyle = text.color;
        if (text.x < text.animation.x) {
        CanvasData.previewCtx.fillText("Hello World", text.animation.x = text.animation.x - text.animation.speed, text.y);
    } else {
        CanvasData.previewCtx.fillText("Hello World", text.x, text.y);
    }
}

export const moveTheAnimationRighToLeftSvg = (svg) => {
    CanvasData.previewCtx.beginPath();
    CanvasData.previewCtx.fillStyle = svg.color;
    if (svg.x < svg.animation.x) {
        CanvasData.previewCtx.rect(svg.animation.x = svg.animation.x - svg.animation.speed, svg.y, svg.width, svg.height);
        CanvasData.previewCtx.fill();
    } else {
        CanvasData.previewCtx.rect(svg.x, svg.y, svg.width, svg.height);
        CanvasData.previewCtx.fill();
    }
}

export const moveTheAnimationRightToLeftImage = (image) => {
    var img = document.getElementById(image.elementId);
    if (image.x < image.animation.x) {
        CanvasData.previewCtx.drawImage(img, image.animation.x = image.animation.x - image.animation.speed, image.y, image.width / 3, image.height / 3);
    } else {
        CanvasData.previewCtx.drawImage(img, image.x, image.y, image.width / 3, image.height / 3);
    }
}

export const moveTheAnimationRightToLeftVideo = (data) => {
    var video = document.getElementById(data.elementId);
    if (data.x < data.animation.x) {
        CanvasData.previewCtx.drawImage(video, data.animation.x = data.animation.x - data.animation.speed, data.y, data.width / 3, data.height / 3);
    } else {
        CanvasData.previewCtx.drawImage(video, data.x, data.y, data.width / 3, data.height / 3);
    }
    video.play()
}