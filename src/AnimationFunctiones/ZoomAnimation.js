import CanvasData from '../CanvasData'

export const zoomText = (text) => {
    if (text.animation.flag === 1) {
        if (text.animation.size < text.animation.zoomTo) {
            CanvasData.previewCtx.font = (text.animation.size = text.animation.size + text.animation.speed) + "px Arial";
            CanvasData.previewCtx.fillStyle = text.color;
            CanvasData.previewCtx.fillText("Hello World", text.x, text.y);
        } else {
            text.animation.flag = 2
        }
    } else {
        if (text.animation.size > text.size) {
            CanvasData.previewCtx.font = (text.animation.size = text.animation.size - text.animation.speed) + "px Arial";
            CanvasData.previewCtx.fillText("Hello World", text.x, text.y);
        } else {
            text.animation.flag = 1
        }
    }
}

export const zoomSvg = (svg) => {
    CanvasData.previewCtx.beginPath();
    CanvasData.previewCtx.fillStyle = svg.color;
    if (svg.animation.flag === 1) {
        if (svg.animation.width < svg.animation.zoomWidth && svg.animation.height < svg.animation.zoomHeight) {
            CanvasData.previewCtx.rect(svg.x, svg.y, svg.animation.width = svg.animation.width + svg.animation.speed, svg.animation.height = svg.animation.height + svg.animation.speed);
            CanvasData.previewCtx.fill();
        } else {
            svg.animation.flag = 2
        }
    } else {
        if (svg.animation.width > svg.width && svg.animation.height > svg.height) {
            CanvasData.previewCtx.rect(svg.x, svg.y, svg.animation.width = svg.animation.width - svg.animation.speed, svg.animation.height = svg.animation.height - svg.animation.speed);
            CanvasData.previewCtx.fill();
        } else {
            svg.animation.flag = 1
        }
    }
}

export const moveTheAnimationTopToBottomImage = (image) => {
    var img = document.getElementById(image.elementId);
    if (image.y > image.animation.y) {
        CanvasData.previewCtx.drawImage(img, image.x, image.animation.y = image.animation.y + image.animation.speed, image.width / 3, image.height / 3);
    } else {
        CanvasData.previewCtx.drawImage(img, image.x, image.y, image.width / 3, image.height / 3);
    }
}

export const moveTheAnimationTopToBottomVideo = (data) => {
    var video = document.getElementById(data.elementId);
    if (data.y > data.animation.y) {
        CanvasData.previewCtx.drawImage(video, data.x, data.animation.y = data.animation.y + data.animation.speed, data.width / 3, data.height / 3);
    } else {
        CanvasData.previewCtx.drawImage(video, data.x, data.y, data.width / 3, data.height / 3);
    }
    video.play()
}