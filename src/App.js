import React from 'react'
import EditScreen from './Components/Screens/EditScreen'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Dimension from './Components/Screens/Dimension'


const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/:id" component={Dimension} />
        <Route exact path="/:id/editor" component={EditScreen} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
