import React from 'react'
import "../Reusable Components Css/Modal.css"

const Modal = (props) => {
    return (
        <div className="modal">
            <canvas id="canvas2" width={props.state.width} height={props.state.height} ></canvas>
        </div>
    )
}

export default Modal
